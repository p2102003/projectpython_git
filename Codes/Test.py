from math import*
from random import*
import numpy as np
import numpy.random as npr
import matplotlib.pyplot as plt
import pandas as pd
from scipy import stats
import Vasicek as v
import unittest

g = v.calibration()


class TestVasicek(unittest.TestCase):
    def test_simul_vasicek(self):
        self.g = v.calibration()
        self.assertEqual(
            type(
                g.vasicek_simulation(
                    0,
                    4,
                    1,
                    2,
                    9,
                    0.3)[0]),
            list)
        self.assertEqual(
            type(
                g.vasicek_simulation(
                    0,
                    4,
                    1,
                    2,
                    9,
                    0.3)[1]),
            list)
        self.assertEqual(
            type(
                g.vasicek_simulation(
                    0,
                    4,
                    1,
                    2,
                    9,
                    0.3)[2]),
            list)

    def test_vasicek_cali(self):
        self.g = v.calibration()
        self.assertEqual(type(g.cali_vasicek_asymp([0.5, 0.7, 0.8])), tuple)
        self.assertEqual(type(g.cali_vasicek_reglin([0.5, 0.7, 0.8])), tuple)
        self.assertEqual(type(g.cali_vasicek_maxvrais([0.5, 0.7, 0.8])), tuple)

    def test_vasicek_prix_oblig(self):
        self.g = v.calibration()
        self.assertEqual(
            type(
                g.prix_oblig(
                    0.03772165,
                    0,
                    2,
                    0.14092182071637027,
                    0.8618486244263984,
                    0.0025232606583656158)),
            float)

    def test_taux(self):
        self.g = v.calibration()
        self.assertEqual(
            type(
                g.taux(
                    0.03772165,
                    0,
                    2,
                    0.14092182071637027,
                    0.8618486244263984,
                    0.0025232606583656158)),
            float)


if __name__ == '__main__':
    unittest.main()
