from math import*
import numpy as np
import numpy.random as npr
import matplotlib.pyplot as plt
from scipy import stats


class calibration:
    def __init__(self):
        """Ce module contient des fonctions utiles
        pour calibrer une courbe des taux zéro-coupon
        grâce au modèle de Vasicek. On peut aussi
        pricer une obligation zéro-coupon en découlant.
        """

    def vasicek_simulation(self, lamb, mu, sigma, r0, N, T):
        """Simule N taux spots selon la dynamique de Vasicek

        Entrées:

        *lamb (float) : taux de réversion moyen
        *mu (float)  :moyenne des taux spots sur le long terme
        *sigma (float) : volatilité des taux spots
        *r0 (float) : taux spot initial
        *N (float):nombre de points simulés
        *T (float) : durée d'observation

        Sorties:

        *rates  (list) : Taux simulés de 0 à N*dt
        *rates[:N:] (list) : Taux simulés de 0 à N*dt-1
        *rates[1::] (list) : Taux simulés de 1 à N*dt
        """
        d_t = T / float(N)
        rates = [r0]
        for i in range(N):
            d_r = lamb * (mu - rates[-1]) * d_t + sigma * npr.normal(0, 1)
            rates.append(rates[-1] + d_r)
        return rates, rates[:N:], rates[1::]

    def regLin_np(self, x, y):
        """
        Ajuste une droite d'équation a*x + b sur les points (x, y) par la méthode
        des moindres carrés.

        Entrées

        * x (list): valeurs de x
        * y (list): valeurs de y


        Sorties:
        * a (float): pente de la droite
        * b (float): ordonnée à l'origine
        * sigma(float): écart-type des erreurs
       """
        # conversion en array numpy
        x = np.array(x)
        y = np.array(y)
        # nombre de points
        npoints = len(x)
        # calculs des parametres a et b
        a = (npoints * (x * y).sum() - x.sum() * y.sum()) / \
            (npoints * (x**2).sum() - (x.sum())**2)
        b = ((x**2).sum() * y.sum() - x.sum() * (x * y).sum()) / \
            (npoints * (x**2).sum() - (x.sum())**2)
        sigma = sqrt(np.var([a * x[k] + b - y[k] for k in range(npoints)]))
        return a, b, sigma

    def cali_vasicek_reglin(self, data, dt=1):
        """
        Ajuste les paramètres du modèle de Vasicek estimés par régression linéaire
        pour un pas de temps dt

        Entrées:

        data(list): jeu de données (une liste comprenant les taux spots)
        dt(float): pas de temps. 1 par défaut

        Sorties:

        *lamb (float) : taux de réversion moyen estimé
        *mu (float)  :moyenne estimée des taux spots sur le long terme
        *sigma (float) : volatilité estimée  des taux spots
        """
        n = len(data)
        x = data[:len(data) - 1:]
        y = data[1::]
        r = self.regLin_np(x, y)
        lamb = (1 - r[0]) / dt
        mu = r[0] / (1 - r[1])
        sigma = r[2] / sqrt(dt)
        return lamb, mu, sigma

    def cali_vasicek_maxvrais(self, data, T=1, N=1):
        """
        Ajuste les paramètres du modèle de Vasicek estimés par maximum de vraisemblance
        pour un pas de temps dt

        Entrées:

        data(list): jeu de données (une liste comprenant les taux spots)
        dt(float): pas de temps. 1 par défaut

        Sorties:

        *lamb (float) : taux de réversion moyen estimé
        *mu (float)  :moyenne estimée des taux spots sur le long terme
        *sigma (float) : volatilité estimée  des taux spots
        """
        data = list(data)
        n = len(data)
        dt = T / N
        x = np.array(data[:len(data) - 1:])
        y = np.array(data[1::])
        num_mu = y.sum() * (x**2).sum() - x.sum() * (x * y).sum()
        den_mu = n * ((x**2).sum() - (x * y).sum()) - \
            (x.sum())**2 + (x.sum() * y.sum())
        mu = num_mu / den_mu
        num_lamb = (x * y).sum() - mu * (y.sum() + x.sum()) + n * (mu)**2
        den_lamb = dt * ((x**2).sum() - 2 * mu * x.sum() + n * (mu)**2)
        lamb = num_lamb / den_lamb
        sigma = sqrt(y.sum() - ((x + lamb * (mu - x) * dt)
                     ** 2).sum()) / sqrt(n * dt)
        return lamb, mu, sigma

    def cali_vasicek_asymp(self, data, alpha=5):
        """
        Ajuste les paramètres du modèle de Vasicek estimés asymptotiquement
        à un seuil de (100-alpha)%

        Entrées:

        data(list): jeu de données (une liste comprenant les taux spots)
        alpha(float): seuil. Par défaut, alpha=5.

        Sorties:

        *lamb (float) : taux de réversion moyen estimé
        *mu (float)  :moyenne estimée des taux spots sur le long terme
        *sigma (float) : volatilité estimée  des taux spots
        """
        n = len(data)
        x = np.array(data[:n - 1:])
        y = np.array(data[1::])
        ecart = (x - y)
        mu = (np.percentile(data, 100 - alpha) +
              np.percentile(data, alpha)) / 2
        lamb = np.var(ecart) * (stats.norm.ppf(1 - alpha / 200) - stats.norm.ppf(alpha / 200)
                                )**2 / (2 * (np.percentile(data, 100 - alpha) - np.percentile(data, alpha))**2)
        sigma = np.std(ecart)
        return lamb, mu, sigma

    def prix_oblig(self, r_0, t, T, lamb, mu, sigma):
        """Calcule le prix en t d'une obligation zero-coupon de maturité T

        Entrées:
        *r_0 (float): taux_spot initial
        *t (float): Date d'observation
        * T (float) : maturité de l'observation
        *lamb (float) : taux de réversion moyen estimé
        *mu (float)  :moyenne estimée des taux spots sur le long terme
        *sigma (float) : volatilité estimée  des taux spots
        """
        while t >= T:
            t = int(input('Saisissez une valeur de t inférieure à T:  '))
        p = sigma**2 / (lamb**2) * (T - t)
        q = -2 * (sigma**2) * (1 - exp(-lamb * (T - t))) / (lamb**3)
        r = (sigma**2) * (1 - exp(-2 * lamb * (T - t))) / (lamb**3)
        prix = exp((p + q + r) / 2) * exp(-mu * (T - t) + (mu - self.vasicek_simulation(
            lamb, mu, sigma, r_0, 1, T)[1][0]) * (1 - exp(-lamb * (T - t))) / lamb)
        return prix

    def taux(self, r_0, t, T, lamb, mu, sigma):
        """Calcule le taux zéro-coupon en t d'une obligation zero-coupon de maturité T

        taux_zero_coupon=-ln(B(t,T))/(T-t) où B(t,T) désigne le prix en t d'une obligation zero-coupon de maturité T

        Entrées:
        *r_0 (float): taux_spot initial
        *t (float): Date d'observation
        * T (float) : maturité de l'observation
        *lamb (float) : taux de réversion moyen estimé
        *mu (float)  :moyenne estimée des taux spots sur le long terme
        *sigma (float) : volatilité estimée  des taux spots
        """
        while t >= T:
            t = int(input('Saisissez une valeur de t inférieure à T: '))
        return -log(self.prix_oblig(r_0, t, T, lamb, mu, sigma)) / (T - t)

    def courbe_taux(self, r_0, t, T, lamb, mu, sigma):
        while t >= T:
            t = int(input('Saisissez une valeur de t inférieure à T: '))
        X = [k for k in range(1, T + 1) if k > t]
        r = [self.taux(r_0, t, x, lamb, mu, sigma) for x in X]
        plt.plot(X, r, label='Courbe de taux zéro-coupon')
        plt.xlabel('Maturité en année')
        plt.ylabel('Taux zero-coupon')
        plt.legend()
        plt.show()
